import 'package:badges/badges.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:untitled/cart.dart';
import 'package:untitled/login.dart';
import 'package:untitled/second_route.dart';


class ItemModel {
  final gambo;
  final text;

  ItemModel(this.gambo, this.text);
}

class HomeScreen extends StatefulWidget {
  final String name;

  HomeScreen({Key key, this.name}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  // variables
  var items = <String>[];
  int _counterItem = 0;

  void _incrementCounter() {
    setState(() {
      _counterItem++;
    });
  }

  TextStyle style =
      TextStyle(fontFamily: 'Montserrat', fontSize: 20.0, color: Colors.white);

  @override
  void initState() {
    // TODO: implement initState
    items = List<String>.generate(10, (i) => "Item $i");
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    // function name (ARGUMENT)
    Widget itemSection(String input1, String input2) {
      return Card(
        elevation: 8.0,
        margin: new EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
        child: Column(
          children: <Widget>[
            ListTile(
              title: Text(input1 + input2),
              leading: Image.network(
                  'http://anthillonline.com/wp-content/uploads/2018/09/eCommerce.jpg'),
              trailing: IconButton(
                icon: Icon(Icons.add_shopping_cart),
                onPressed: _incrementCounter,
              ),
            ),
          ],
        ),
      );
    }

    //Method yang digunakan supaya screen tidak akan patah balik ke belakang
    return new WillPopScope(
        onWillPop: () => _exitApp(context),
        child: Scaffold(
            appBar: AppBar(
              backgroundColor: Colors.green,
              title: Text('List of Item'),
              leading: MaterialButton(
                child: Text('LogOut', style: TextStyle(fontSize: 13,color: Colors.white)),
                onPressed: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context) => Untitled()));
                },
              ),
              actions: <Widget>[
                BadgeIconButton(
                  itemCount: _counterItem,
                  icon: Icon(Icons.shopping_cart),
                  onPressed: () {
                    Navigator.push(context, MaterialPageRoute(builder: (context) => Cart()));
                  },
                ),
              ],
            ),
            body: ListView.builder(
              itemCount: items.length,
              itemBuilder: (context, i) {
                return itemSection('Items ', '$i');
              },
            )));
  }
}

_exitApp(BuildContext context) {}

class SignRoute extends StatelessWidget {
  final String signUpRoute;

  const SignRoute({Key key, this.signUpRoute}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text('Data of SignUp user'),
      ),
      body: Center(
        child: RaisedButton(
          onPressed: () {
            Navigator.pop(context);
            MaterialPageRoute(builder: (context) => SignUp());
          },
          child: Text(signUpRoute),
        ),
      ),
    );
  }
}
