import 'package:flutter/material.dart';
import 'package:untitled/generated/third_route.dart';
import 'package:untitled/second_route.dart';
import 'package:untitled/shared_pref.dart';

// IMPORTANT TO CALL THE CLASS!!!

class Untitled extends StatefulWidget {
  final String title;

  Untitled({Key key, this.title}) : super(key: key);

  @override
  _UntitledState createState() => _UntitledState();
}

class _UntitledState extends State<Untitled> {
  // function for text controller and text style
  //TextEditingController digunakan untuk user write on the space given.User
  // masuk input
  final myController = TextEditingController();
  TextStyle style =
      TextStyle(fontFamily: 'MontSerrat', fontSize: 20.0, color: Colors.white);

  //SharedPreferences variable


  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    //username field
    final usernameField = TextField(
      obscureText: false,
      controller: myController,
      style: style,
      decoration: InputDecoration(
        hintText: 'Email Address',
        contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
    );

    //password field
    final passwordField = TextField(
      obscureText: true,
      style: style,
      decoration: InputDecoration(
          hintText: 'Password',
          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          border:
              OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
    );

    //SharedPreferences variable



    //SignIn field
    final loginButton = Material(
      elevation: 5.0,
      borderRadius: BorderRadius.circular(30.0),
      //nie color biru
      color: Color(0xff01A0C7),
      child: MaterialButton(
        minWidth: MediaQuery.of(context).size.width,
        padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        onPressed: () {
          saveEmail(myController.text);
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => HomeScreen(
                        name: myController.text,
                      )));
        },
        child: Text(
          'SIGNIN',
          textAlign: TextAlign.center,
          style: style.copyWith(
              color: Color.fromRGBO(202, 46, 131, 1),
              fontWeight: FontWeight.bold),
        ),
      ),
    );

    //SignUp field
    final signUp = GestureDetector(
        onTap: () {
          Navigator.push(
              context, MaterialPageRoute(builder: (context) =>SignUp()));
        },
        child: Text(
          'DONT HAVE AN ACCOUNT?' 'SIGN UP',
          textAlign: TextAlign.center,
          style: style.copyWith(
              color: Colors.white, fontWeight: FontWeight.bold, fontSize: 12.0),
        ));

    //forgot pass
    final forgotPassword = Text(
      'Forgot Password?',
      textAlign: TextAlign.left,
      style: style.copyWith(
          color: Colors.white, fontWeight: FontWeight.bold, fontSize: 14.0),
    );

    Widget titleSection = Container(
      padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
      child: Row(
        children: <Widget>[
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  child: Text(
                      'SIGN IN',
                      style:
                      TextStyle(fontWeight: FontWeight.bold, fontSize: 25.0,
                          color: Colors.white)),
                  padding: EdgeInsets.only(top: 50.0),
                ),
              ],
            ),
          ),
        ],
      ),
    );

    return Scaffold(
      //Used stack to vary the contain of the widget
      body: Stack(
        children: [
          Container(
              decoration: BoxDecoration(
                gradient: LinearGradient(colors: [
                  Color.fromRGBO(188, 49, 145, 1),
                  Color.fromRGBO(109, 72, 162, 1),
                ]),
              )),
          ListView(
            children: <Widget>[
              Image.network(
                  'http://anthillonline.com/wp-content/uploads/2018/09/eCommerce.jpg',
                  width: 1000,
                  height: 300,
                  fit: BoxFit.cover),
              titleSection,
              Container(
                child: Padding(
                  padding: const EdgeInsets.all(30.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      SizedBox(height: 6.0),
                      usernameField,
                      SizedBox(height: 20.0),
                      passwordField,
                      SizedBox(height: 35.0),
                      forgotPassword,
                      SizedBox(height: 15.0),
                      loginButton,
                      SizedBox(height: 15.0),
                      signUp,
                      SizedBox(height: 60.0),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
