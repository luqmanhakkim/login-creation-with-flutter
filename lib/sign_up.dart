import 'package:flutter/material.dart';
import 'package:untitled/generated/third_route.dart';
import 'package:untitled/main.dart';

class Second extends StatefulWidget {
  final String space;

  Second({Key key, this.space}) : super(key: key);

  @override
  _SecondState createState() => _SecondState();
}

class _SecondState extends State<Second> {
  TextStyle styles =
      TextStyle(fontFamily: 'MontSerrat', fontSize: 20.0, color: Colors.white);
  final controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    final nameField = TextField(
      obscureText: false,
      controller: controller,
      style: styles,
      decoration: InputDecoration(
        hintText: 'Your Name',
        contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
    );

    final passField = TextField(
      obscureText: true,
      style: styles,
      decoration: InputDecoration(
        hintText: 'Password',
        contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
    );

    final emailField = TextField(
      obscureText: false,
      style: styles,
      decoration: InputDecoration(
        hintText: 'Email Address',
        contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
    );

    final mobileField = TextField(
      obscureText: false,
      style: styles,
      decoration: InputDecoration(
        hintText: 'Mobile Number',
        contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
    );

    final signButton = Material(
      elevation: 5.0,
      borderRadius: BorderRadius.circular(32.0),
      color: Color(0xff01A0C7),
      child: MaterialButton(
        minWidth: MediaQuery.of(context).size.width,
        padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        onPressed: () {
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => SignRoute(
            signUpRoute: controller.text,
          )));
        },
        child: Text(
          'SignUP',
          textAlign: TextAlign.center,
          style: styles.copyWith(
              color: Color.fromRGBO(202, 46, 131, 1),
              fontWeight: FontWeight.bold),
        ),
      ),
    );

    final signIn = GestureDetector(
      onTap: () {
        //Navigator pop dia pergi kebelakang
        //Navigator push dia pergi kedepan
        Navigator.pop(
            context, MaterialPageRoute(builder: (context) => MyApp()));
      },
      child: Text('IF YOU HAVE AN ACCOUNT?' 'SIGN IN',
      style: styles.copyWith(
        color: Colors.white, fontWeight: FontWeight.bold, fontSize: 12.0),
      ));


    return Container(
      child: Padding(
        padding: const EdgeInsets.all(30.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            SizedBox(height: 2.0),
            nameField,
            SizedBox(height: 15.0),
            passField,
            SizedBox(height: 15.0),
            emailField,
            SizedBox(height: 15.0),
            mobileField,
            SizedBox(height: 15.0),
            signButton,
            SizedBox(height: 15.0),
            signIn,
            SizedBox(height: 50.0),
          ],
        ),
      ),
    );
  }
}
