import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:untitled/login.dart';
import 'package:untitled/second_route.dart';
import 'package:untitled/shared_pref.dart';

import 'generated/third_route.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {

//Material App hanya boleh guna sekali je dalam APP
  //Return kan class yang penting dalam Material App
    return MaterialApp(
      //Material App return home (first screen) sahaja

      //Future builder used for return widget when we have future function
        home: FutureBuilder<String>(
          future: getEmail(),
          builder: (context, snapshot){
            if (snapshot.hasData) {
              return HomeScreen();
            }
            return Untitled();
          },
        ));
  }
}
