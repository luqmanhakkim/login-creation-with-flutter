import 'package:flutter/material.dart';
import 'package:untitled/sign_up.dart';

class SignUp extends StatefulWidget {

  @override
  SignUpState createState() => new SignUpState();
  }

  class SignUpState extends State<SignUp> {


  @override
  Widget build(BuildContext context) {
    Widget signUpSection = Container(
      padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
      child: Row(
        children: <Widget>[
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  child: Text('Sign Up',
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 25.0,
                          color: Colors.white)),
                  padding: EdgeInsets.only(top: 50.0),
                ),
              ],
            ),
          ),
        ],
      ),
    );

    return Scaffold(
      body: Stack(
        children: <Widget>[
          Container(
              decoration: BoxDecoration(
            gradient: LinearGradient(colors: [
              Color.fromRGBO(188, 49, 145, 1),
              Color.fromRGBO(109, 72, 162, 1),
            ]),
          )),
          ListView(
            children: <Widget>[
              Image.network(
                  'http://anthillonline.com/wp-content/uploads/2018/09/eCommerce.jpg',
                  width: 1000,
                  height: 300,
                  fit: BoxFit.cover),
              signUpSection,
              Second()
            ],
          ),
        ],
      ),
    );
  }
}


