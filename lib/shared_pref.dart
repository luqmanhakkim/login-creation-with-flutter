import 'package:shared_preferences/shared_preferences.dart';


//Future function is a value that does not come directly and instantly
//Shared preferences react as read and write and come back to assign value

Future<String> getEmail () async {
  final SharedPreferences prefs = await SharedPreferences.getInstance();
  String email = prefs.getString('save');
  return email;
}

Future<void> saveEmail(String email) async {
  final SharedPreferences prefs = await SharedPreferences.getInstance();
  prefs.setString('save', email);
}
